const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.locals.students = [
    {
        name: "Gigel",
        surname: "Popel",
        age: 23
    },
    {
        name: "Gigescu",
        surname: "Ionel",
        age: 25
    }
];

app.get('/students', (req, res) => {
    res.status(200).json(app.locals.products);
});

app.post('/students', (req, res, next) => {
    if(req.body.constructor === Object && Object.keys(req.body).length === 0 ){
        res.status(500).json({message:"Body is missing"});
    }else if(req.body.name === undefined || req.body.surname === undefined || req.body.age === undefined) {
        res.status(500).json({message:"Invalid body format"});
    }else if(req.body.age < 0 ){
        res.status(500).json({message: "Age should be a positive number"});
    }else {
        let student = {
            name: req.body.name,
            surname: req.body.surname,
            age: req.body.age
        };
        
        if(isStudentAlreadyExist(student)){
            res.status(500).json({message:"Student already exists"});
        }else{
            app.locals.students.push(student);
            res.status(201).json({message:"Created"});
        }
    }
    
})

function isStudentAlreadyExist(student){
    let found = false;
    for(var i = 0; i < app.locals.students.length; i++) {
           if (app.locals.students[i].name === student.name && app.locals.students[i].surname === student.surname && app.locals.students[i].age === student.age) {
            found = true;
            break;
        }
    }
    return found;
    //&& app.locals.students[i].surname === student.surname && app.locals.students[i].age === student.age
}

module.exports = app;