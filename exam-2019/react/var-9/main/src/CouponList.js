// import React from 'react';

// export class CouponList extends React.Component {
//     constructor(){
//         super();
//         this.state = {
//             coupons: []
//         };
//     }

//     render(){
//         return(
//             <div>
                
//             </div>
//         )
//     }
// }
import React from 'react';
import AddCoupon from './AddCoupon';

export class CouponList extends React.Component {
    constructor(){
        super();
        this.state = {
            coupons: []
        };
        this.addElementToList = this.addElementToList.bind(this);
    }
    
    addElementToList(element){
        this.state.coupons.push(element);
        this.setState({coupons: this.state.coupons});
    }

    render(){
        return(
            <div>
                <ul>
                    { 
                    this.state.coupons.map(
                        (coupon, idx) => 
                        ( <li key={idx}> Category: {coupon.category} Discount: {coupon.discount} Availability: {coupon.availability} </li> )
                        
                    )}
                </ul>
                <AddCoupon onAdd={this.addElementToList}/>
            </div>
        )
    }
}

export default CouponList;