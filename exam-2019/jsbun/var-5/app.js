function textProcessor(input, tokens) {
    if (typeof input !== 'string') {
        throw new Error('Input should be a string');
    }
    if (input.length < 6) {
        throw new Error('Input should have at least 6 characters');
    }
    let isValidArray = true;
    for (let token of tokens) {
        if (typeof token.tokenName !== "string") {
            isValidArray = false;
            break;
        }
    }
    if (isValidArray == false) {
        throw new Error("Invalid array format");
    }

    if (input.includes("${name}") == false && input.includes("${surname}") == false) {
        return input;
    }
    else {
        // let indexOfToken =0;
        //         let inputs = input.split(' ');
        //         for(var i=0; i<inputs.length; i++){
        //             if(inputs[i] === "${name}"){
        //                 inputs[i]=tokens[indexOfToken].tokenValue;
        //                 indexOfToken++;
        //             }
        //             else if(inputs[i] === "${surname}."){
        //                 inputs[i]=tokens[indexOfToken].tokenValue+".";
        //                 indexOfToken++;
        //             }
        //         }
        //         console.log(inputs.join(' '));
        //         return inputs.join(' ');

        var result = input.split(' ')

        for (let i = 0; i < result.length; i++) {
            if (result[i].startsWith('${')) {

                let afterThat = result[i].substring(result[i].indexOf('}') + 1, result[i].length)

                tokens.forEach((token) => {
                    if (token.tokenName == result[i].substring(result[i].indexOf('{') + 1, result[i].indexOf('}'))) {
                        result[i] = token.tokenValue

                    }

                });

                if (afterThat.length > 0) {
                    result[i] = result[i] + afterThat

                }

            }

        }

        return result.join(' ')


    }
}

const app = {
    textProcessor: textProcessor
};

module.exports = app;
