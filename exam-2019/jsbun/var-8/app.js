function addTokens(input, tokens) {
    if (typeof input !== "string") {
        throw new Error("Invalid input");
    }
    else if (input.length < 6) {
        throw new Error("Input should have at least 6 characters");
    }
    else {
        let isValidArray = true;
        for (let token of tokens) {
            if (typeof token.tokenName !== "string") {
                isValidArray = false;
                break;
            }
        }
        if (isValidArray == false) {
            throw new Error("Invalid array format");
        }
        else {
            if (input.includes("...") == false) {
                return input;
            }
            else {
                let indexOfToken = 0;
                let inputs = input.split(' ');
                for (var i = 0; i < inputs.length; i++) {
                    if (inputs[i] === "...") {
                        inputs[i] = "${" + tokens[indexOfToken].tokenName + "}";
                        indexOfToken++;
                    }
                }

                console.log(inputs.join(' '));
                return inputs.join(' ');
            }
        }
    }

}


const app = {
    addTokens: addTokens
}

module.exports = app;
