// jsontable.js

document.addEventListener("DOMContentLoaded", ()=>{
     function loadJSON(callback) {

            var xobj = new XMLHttpRequest();
            xobj.overrideMimeType("application/json");
            xobj.open('GET', 'invoice.json', true); // Replace 'my_data' with the path to your file
            xobj.onreadystatechange = function() {
                if (xobj.readyState == 4 && xobj.status == "200") {
                    // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
                    callback(xobj.responseText);
                }
            };
            xobj.send(null);
        }
        var actual_JSON;

        function init() {
            loadJSON(function(response) {
                // Parse JSON string into object
                actual_JSON = JSON.parse(response);
                console.log(actual_JSON);
            });
        }
        init();
//   let jsonstr=`[
//     {"id":"2", "first_name":"Sam","last_name":"Smith", "phone":"111-222-3333","email":"ssmith@yahoo.com","address":"33 Birch Rd","city":"Miami","state":"FL"},
//     {"id":"3", "first_name":"Brad","last_name":"Traversy", "phone":"211-322-4333","email":"brad@test.com","address":"222 South St","city":"Portland","state":"FL"}
//   ]`

let jsonstr=actual_JSON;
  let jsonobj=JSON.parse(jsonstr)
  let form=document.querySelector("form")
form.string.addEventListener("click", ()=>{
    let out=form.out
    if(!jsonobj.length)
      return
    let tblstr="<table>"
    tblstr+="<caption>Table Using HTML String</caption>"
    tblstr+="<tr>"
    for(let prop in jsonobj[0]) {
      tblstr+=`<th>${prop}`
    }
    tblstr+=jsonobj.reduce((s, x) => {
      s+="<tr>"
      for(let prop in x) {
        s+=`<td>${x[prop]}`
      }
      return s
    }, "")
    tblstr+="</table>"
    out.insertAdjacentHTML("beforeend", "<p>" + tblstr)
  })
  form.dom.addEventListener("click", ()=>{
    let out=form.out
    if(!jsonobj.length)
      return
    let tbl=document.createElement("table")
    tbl.createCaption().innerText="Table Using DOM API"
    let hdr=tbl.insertRow()
    for(let prop in jsonobj[0]) {
      hdr.appendChild(document.createElement("th")).innerText=prop
    }
    jsonobj.reduce((t, x) => {
      let row=t.insertRow()
      for(let prop in x) {
        row.insertCell().innerText=x[prop]
      }
      return t
    }, tbl)
    out.appendChild(document.createElement("p"))
    out.appendChild(tbl)
  })
  form.clear.addEventListener("click", ()=>form.out.innerHTML="")
})